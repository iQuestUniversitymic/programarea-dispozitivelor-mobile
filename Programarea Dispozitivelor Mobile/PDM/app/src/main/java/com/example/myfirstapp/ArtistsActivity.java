package com.example.myfirstapp;

import android.arch.lifecycle.ViewModelProviders;
import android.content.Context;
import android.content.Intent;
import android.net.ConnectivityManager;
import android.os.Build;
import android.support.annotation.RequiresApi;
import android.support.v4.view.ViewPager;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.util.Log;
import android.widget.Toast;

import com.example.myfirstapp.Database.ArtistViewModel;
import com.example.myfirstapp.model.Artist;
import com.example.myfirstapp.model.ArtistDTO;
import com.example.myfirstapp.service.APIService;

import java.util.ArrayList;
import java.util.List;
import java.util.Timer;
import java.util.TimerTask;
import java.util.concurrent.ExecutionException;
import java.util.concurrent.TimeUnit;

import okhttp3.OkHttpClient;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;
import retrofit2.Retrofit;
import retrofit2.converter.gson.GsonConverterFactory;

public class ArtistsActivity extends AppCompatActivity {

    private static final String TAG = "ArtistsActivity";
    private String authToken;

    private SectionsStatePageAdapter mSectionsStatePageAdapter;
    private ViewPager mViewPager;

    OkHttpClient client = new OkHttpClient.Builder()
            .connectTimeout(10, TimeUnit.SECONDS)
            .writeTimeout(10, TimeUnit.SECONDS)
            .readTimeout(10, TimeUnit.SECONDS)
            .build();

    Retrofit.Builder builder = new Retrofit.Builder()
            .baseUrl("http://192.168.43.125:44397/")
            .client(client)
            .addConverterFactory(GsonConverterFactory.create());

    Retrofit retrofit = builder.build();

    APIService APIService = retrofit.create(APIService.class);

    ArrayList<String> artistsFromServer = new ArrayList<>();

    TimerTask timerTask;
    Timer timer;

    @RequiresApi(api = Build.VERSION_CODES.N)
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_artists);
        Log.d(TAG, "onCreate: Started.");

        Intent intent = getIntent();
        authToken = intent.getExtras().getString("authToken");

        mSectionsStatePageAdapter = new SectionsStatePageAdapter(getSupportFragmentManager());
        mViewPager = findViewById(R.id.container);

        ArtistViewModel artistViewModel = ViewModelProviders.of(this).get(ArtistViewModel.class);

        FragmentAdd fragmentAdd = new FragmentAdd();
        fragmentAdd.setAuthToken(authToken);
        fragmentAdd.setAPIService(APIService);
        fragmentAdd.setArtistViewModel(artistViewModel);

        setupViewPager(mViewPager, fragmentAdd);
        try {
            getData(authToken, artistViewModel);
        } catch (ExecutionException e) {
            e.printStackTrace();
        } catch (InterruptedException e) {
            e.printStackTrace();
        }

        timerTask = new TimerTask() {
            boolean isConnected;

            @Override
            public void run() {

                isConnected = false;

                ConnectivityManager connectivityManager = (ConnectivityManager) getSystemService(Context.CONNECTIVITY_SERVICE);
                if (connectivityManager.getActiveNetworkInfo() != null
                        && connectivityManager.getActiveNetworkInfo().isConnected()
                        && connectivityManager.getActiveNetworkInfo().isAvailable()) {

                    isConnected = true;
                    try {
                        synchronizeData(authToken, artistViewModel);
                    } catch (ExecutionException e) {
                        System.out.println(e.getMessage());
                    } catch (InterruptedException e) {
                        System.out.println(e.getMessage());
                    }
                }

                System.out.println(isConnected);
            }
        };

        timer = new Timer();
        timer.scheduleAtFixedRate(timerTask, 1000, 20000);
    }

    private void setupViewPager(ViewPager viewPager, FragmentAdd fragmentAdd) {
        SectionsStatePageAdapter adapter = new SectionsStatePageAdapter(getSupportFragmentManager());

        adapter.addFragment(new FragmentMenu(), "FragmentMenu");
        adapter.addFragment(new FragmentIdols(), "FragmentIdols");
        adapter.addFragment(fragmentAdd, "FragmentAdd");

        viewPager.setAdapter(adapter);
    }

    public void setViewPager(int fragmentNumber) {
        mViewPager.setCurrentItem(fragmentNumber);
    }

    private synchronized void getData(String authToken, ArtistViewModel artistViewModel) throws ExecutionException, InterruptedException {
        Call<List<Artist>> call = APIService.getArtists("Bearer " + authToken);
        artistViewModel.deleteAll();

        System.out.println(artistViewModel.getArtistsFromDb().size());

        call.enqueue(new Callback<List<Artist>>() {

            @RequiresApi(api = Build.VERSION_CODES.N)
            @Override
            public void onResponse(Call<List<Artist>> call, Response<List<Artist>> response) {
                if (response.isSuccessful()) {
                    Toast.makeText(ArtistsActivity.this, "Got data!", Toast.LENGTH_SHORT).show();
                    response.body().forEach( artist -> artistViewModel.add(artist.getArtistName()));
                    response.body().forEach(artist -> artistsFromServer.add(artist.getArtistName().toLowerCase()));
                } else {
                    Toast.makeText(ArtistsActivity.this, "No data!", Toast.LENGTH_SHORT).show();
                }
            }

            @Override
            public void onFailure(Call<List<Artist>> call, Throwable t) {
                System.out.println(t.getCause());
                Toast.makeText(ArtistsActivity.this, "An error ocurred while retrieving data!", Toast.LENGTH_SHORT).show();
            }
        });
    }

    @RequiresApi(api = Build.VERSION_CODES.N)
    private synchronized void synchronizeData(String authToken, ArtistViewModel artistViewModel) throws ExecutionException, InterruptedException {

        getDataFromServer(authToken);
        artistViewModel.getArtistsFromDb().forEach(artist -> {

            if (!artistsFromServer.contains(artist.getArtistName().toLowerCase())) {
                System.out.println(artist.writeDetails() + "nu e postat");
                Call<Void> call = APIService.addArtist("Bearer " + authToken, new ArtistDTO(artist.getArtistName()));
                call.enqueue(new Callback<Void>() {

                    @RequiresApi(api = Build.VERSION_CODES.N)
                    @Override
                    public void onResponse(Call<Void> call, Response<Void> response) {
                        if (response.isSuccessful()) {
                            artistsFromServer.add(artist.getArtistName().toLowerCase());
                        } else {
                            System.out.println("not adding");
                        }
                    }
                    @Override
                    public void onFailure(Call<Void> call, Throwable t) {
                        System.out.println(t.getCause());
                        System.out.println(t.getLocalizedMessage());
                        System.out.println(t.getStackTrace());
                        System.out.println(t.getMessage());
                    }
                });
            }

        });
    }

    private synchronized void getDataFromServer(String authToken) {
        Call<List<Artist>> call = APIService.getArtists("Bearer " + authToken);
        call.enqueue(new Callback<List<Artist>>() {

            @RequiresApi(api = Build.VERSION_CODES.N)
            @Override
            public void onResponse(Call<List<Artist>> call, Response<List<Artist>> response) {
                if (response.isSuccessful()) {
  //                  Toast.makeText(ArtistsActivity.this, "Got data from server!", Toast.LENGTH_SHORT).show();
                    artistsFromServer.clear();
                    response.body().forEach(artist -> artistsFromServer.add(artist.getArtistName().toLowerCase()));
                    System.out.println("Am adus de pe server: --->");
                    System.out.println(response.body().size() + "artisti");
                } else {
                    Toast.makeText(ArtistsActivity.this, "No data!", Toast.LENGTH_SHORT).show();
                }
            }

            @Override
            public void onFailure(Call<List<Artist>> call, Throwable t) {
                System.out.println(t.getCause());
                Toast.makeText(ArtistsActivity.this, "An error ocurred while retrieving data from server!", Toast.LENGTH_SHORT).show();
            }
        });
    }


    @Override
    protected void onDestroy() {
        super.onDestroy();
        timer.cancel();
    }
}
