package com.example.myfirstapp.service;

import android.arch.lifecycle.LiveData;
import android.arch.persistence.room.Dao;
import android.arch.persistence.room.Delete;
import android.arch.persistence.room.Insert;
import android.arch.persistence.room.Query;
import android.arch.persistence.room.Update;

import com.example.myfirstapp.model.Artist;

import java.util.List;

@Dao
public interface ArtistsDao {

    @Insert
    void insert(Artist artist);

    @Update
    void update(Artist artist);

    @Query("DELETE FROM artist_table WHERE artistId = :id")
    void delete(int id);

    @Query("DELETE FROM artist_table")
    void deleteAll();

    @Query("SELECT * FROM artist_table")
    LiveData<List<Artist>> getAll();

    @Query("SELECT COUNT(*) FROM (SELECT * FROM artist_table WHERE artistId = :id AND artistName LIKE :artistName)")
    int contains(int id, String artistName);


    @Query("SELECT MAX( artistId ) FROM artist_table")
    int lastIndex();

    @Query("SELECT COUNT(*) FROM (SELECT * FROM artist_table WHERE LOWER(artistName) LIKE :artistName)")
    int containsName(String artistName);

    @Query("SELECT * FROM artist_table")
    List<Artist> getAllData();


}
