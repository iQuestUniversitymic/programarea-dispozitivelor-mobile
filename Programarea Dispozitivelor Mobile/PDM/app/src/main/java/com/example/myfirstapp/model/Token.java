package com.example.myfirstapp.model;

public class Token {

    private String token;
    private String expirationDate;


    public Token(String token, String expirationDate) {
        this.token = token;
        this.expirationDate = expirationDate;
    }

    public Token(String token){
        this.token = token;
        this.expirationDate = "";
    }

    public Token(){

    }

    public String getToken() {
        return token;
    }

    public void setToken(String token) {
        this.token = token;
    }

    public String getExpirationDate() {
        return expirationDate;
    }

    public void setExpirationDate(String expirationDate) {
        this.expirationDate = expirationDate;
    }
}
