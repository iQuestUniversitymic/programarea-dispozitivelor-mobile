package com.example.myfirstapp;

import android.arch.lifecycle.ViewModelProviders;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import com.example.myfirstapp.Database.ArtistViewModel;
import com.example.myfirstapp.model.Artist;

import java.util.ArrayList;
import java.util.List;
import java.util.Objects;

public class FragmentIdols extends Fragment {

    private ArtistViewModel artistViewModel;

    @Nullable
    @Override
    public View onCreateView(@NonNull LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {

        View view = inflater.inflate(R.layout.fragment_idols_layout, container, false);

        RecyclerView recyclerView = view.findViewById(R.id.recycler_view);
        recyclerView.setLayoutManager(new LinearLayoutManager(getActivity()));
        recyclerView.setHasFixedSize(true);

        final ArtistAdapter adapter = new ArtistAdapter();
        recyclerView.setAdapter(adapter);

        artistViewModel = ViewModelProviders.of(Objects.requireNonNull(getActivity())).get(ArtistViewModel.class);

        artistViewModel.getAll().observe(this, adapter::setArtists);

        return view;
    }


}
