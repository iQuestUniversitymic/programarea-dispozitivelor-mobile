package com.example.myfirstapp.service;

import com.example.myfirstapp.model.Artist;
import com.example.myfirstapp.model.ArtistDTO;
import com.example.myfirstapp.model.Token;
import com.example.myfirstapp.model.User;
import com.example.myfirstapp.model.UserDTO;

import java.io.Serializable;
import java.util.List;

import retrofit2.Call;
import retrofit2.http.Body;
import retrofit2.http.GET;
import retrofit2.http.Header;
import retrofit2.http.POST;

public interface APIService {

    @POST("auth/login")
    Call<Token> login(@Body UserDTO userCredentials);

    @GET("artists")
    Call<List<Artist>> getArtists(@Header("Authorization") String authToken);

    @POST("artists")
    Call<Void> addArtist(@Header("Authorization") String authToken, @Body ArtistDTO artistDTO);
}
