package com.example.myfirstapp;

import android.content.Context;
import android.net.ConnectivityManager;
import android.os.Build;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.support.annotation.RequiresApi;
import android.support.v4.app.Fragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.TextView;
import android.widget.Toast;

import com.example.myfirstapp.Database.ArtistViewModel;
import com.example.myfirstapp.model.ArtistDTO;
import com.example.myfirstapp.service.APIService;

import java.util.ArrayList;
import java.util.concurrent.ExecutionException;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

public class FragmentAdd extends Fragment {

    private String authToken;
    private APIService APIService;
    private ArtistViewModel artistViewModel;

    @RequiresApi(api = Build.VERSION_CODES.N)
    @Nullable
    @Override
    public View onCreateView(@NonNull LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {

        View view = inflater.inflate(R.layout.fragment_add_layout, container, false);

        Button addButton = view.findViewById(R.id.button2);
        addButton.setOnClickListener((v) -> {
            try {
                addArtist();
            } catch (ExecutionException e) {
                e.printStackTrace();
            } catch (InterruptedException e) {
                e.printStackTrace();
            }
        });

        return view;
    }

    public void setAuthToken(String authToken) {
        this.authToken = authToken;
    }

    public void setAPIService(APIService APIService) {
        this.APIService = APIService;
    }

    public void setArtistViewModel(ArtistViewModel artistViewModel) {
        this.artistViewModel = artistViewModel;
    }

    @RequiresApi(api = Build.VERSION_CODES.N)
    private synchronized void addArtist() throws ExecutionException, InterruptedException {

        TextView artistNameTextView = getView().findViewById(R.id.editText);
        String artistName = artistNameTextView.getText().toString();

        if (artistName.equals("")) {
            Toast.makeText(getActivity(), "Your idol's name is empty!", Toast.LENGTH_SHORT).show();
            return;
        }

        if (artistViewModel.containsArtist(artistName)) {
            Toast.makeText(getActivity(), "Your idol already exists in the list!", Toast.LENGTH_SHORT).show();
            return;
        }

        if (isConnected()) {
            addArtist(artistName);
        } else {
            artistViewModel.add(artistName);
            Toast.makeText(getActivity(), "New idol on the list!", Toast.LENGTH_SHORT).show();
        }
    }

    private synchronized void addArtist(String artistName) {
        Call<Void> call = APIService.addArtist("Bearer " + authToken, new ArtistDTO(artistName));
        call.enqueue(new Callback<Void>() {

            @RequiresApi(api = Build.VERSION_CODES.N)
            @Override
            public void onResponse(Call<Void> call, Response<Void> response) {
                if (response.isSuccessful()) {
                    artistViewModel.add(artistName);
                    Toast.makeText(getActivity(), "New idol on the list!", Toast.LENGTH_SHORT).show();
                } else {
                    Toast.makeText(getActivity(), "No idol inserted!", Toast.LENGTH_SHORT).show();
                }
            }

            @Override
            public void onFailure(Call<Void> call, Throwable t) {
                System.out.println(t.getCause());
                System.out.println(t.getLocalizedMessage());
                System.out.println(t.getStackTrace());
                System.out.println(t.getMessage());
                Toast.makeText(getActivity(), "An error ocurred while posting data!", Toast.LENGTH_SHORT).show();
            }
        });
    }

    private synchronized boolean isConnected() {

        ConnectivityManager connectivityManager = (ConnectivityManager) getActivity().getSystemService(Context.CONNECTIVITY_SERVICE);
        if (connectivityManager.getActiveNetworkInfo() != null
                && connectivityManager.getActiveNetworkInfo().isConnected()
                && connectivityManager.getActiveNetworkInfo().isAvailable()) {
            return true;
        }

        return false;
    }

}
