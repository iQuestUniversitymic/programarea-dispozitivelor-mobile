package com.example.myfirstapp.Database;

import android.arch.persistence.db.SupportSQLiteDatabase;
import android.arch.persistence.room.Database;
import android.arch.persistence.room.Room;
import android.arch.persistence.room.RoomDatabase;
import android.content.Context;
import android.support.annotation.NonNull;

import com.example.myfirstapp.model.Artist;
import com.example.myfirstapp.service.ArtistsDao;


@Database(entities = {Artist.class}, version = 2)
public abstract class ArtistDatabase extends RoomDatabase {

    private static ArtistDatabase instance;

    public abstract ArtistsDao artistsDao();

    public static synchronized ArtistDatabase getInstance(Context context) {

        if (instance == null) {
            instance = Room.databaseBuilder(context.getApplicationContext(),
                    ArtistDatabase.class, "artist_database")
                    .fallbackToDestructiveMigration()
                    .build();
        }
        return instance;
    }
}
