package com.example.myfirstapp.Database;

import android.app.Application;
import android.arch.lifecycle.LiveData;
import android.os.AsyncTask;

import com.example.myfirstapp.model.Artist;
import com.example.myfirstapp.service.ArtistsDao;

import java.util.List;
import java.util.concurrent.ExecutionException;

public class ArtistRepository {

    private ArtistsDao artistsDao;
    private LiveData<List<Artist>> allArtists;

    public ArtistRepository(Application application) {
        ArtistDatabase artistDatabase = ArtistDatabase.getInstance(application);
        artistsDao = artistDatabase.artistsDao();
        allArtists = artistsDao.getAll();
    }

    public synchronized void insert(Artist artist) {
        new InsertArtistAsyncTask(artistsDao).execute(artist);
    }

    public void update(Artist artist) {
        new UpdateArtistAsyncTask(artistsDao).execute(artist);

    }

    public void delete(Artist artist) {
        new DeleteArtistAsyncTask(artistsDao).execute(artist);

    }

    public synchronized void deleteAll() {
        new DeleteAllArtistAsyncTask(artistsDao).execute();
    }

    public synchronized void add(String artistName) {
        new AddArtistAsyncTask(artistsDao).execute(artistName);
    }

    public synchronized LiveData<List<Artist>> getAll() {
        return allArtists;
    }

    public synchronized List<Artist> getAllData() throws ExecutionException, InterruptedException {
        return new GetAllArtistsAsyncTask(artistsDao).execute().get();
    }

    public synchronized Boolean contains(String artistName) throws ExecutionException, InterruptedException {
        return new ContainsArtistAsyncTask(artistsDao).execute(artistName).get();
    }


    private static class InsertArtistAsyncTask extends AsyncTask<Artist, Void, Void> {

        private ArtistsDao artistsDao;

        InsertArtistAsyncTask(ArtistsDao artistsDao) {
            this.artistsDao = artistsDao;
        }

        @Override
        protected Void doInBackground(Artist... artists) {
            if (artistsDao.contains(artists[0].getArtistId(), artists[0].getArtistName()) == 0) {
                artistsDao.insert(artists[0]);
            }
            return null;
        }
    }

    private static class AddArtistAsyncTask extends AsyncTask<String, Void, Void> {

        private ArtistsDao artistsDao;

        AddArtistAsyncTask(ArtistsDao artistsDao) {
            this.artistsDao = artistsDao;
        }

        @Override
        protected Void doInBackground(String... strings) {
            if (artistsDao.containsName(strings[0]) == 0) {
                artistsDao.insert(new Artist(artistsDao.lastIndex() + 1, strings[0]));
            }
            return null;
        }
    }

    private class UpdateArtistAsyncTask extends AsyncTask<Artist, Void, Void> {

        private ArtistsDao artistsDao;

        UpdateArtistAsyncTask(ArtistsDao artistsDao) {
            this.artistsDao = artistsDao;
        }

        @Override
        protected Void doInBackground(Artist... artists) {
            artistsDao.update(artists[0]);
            return null;
        }
    }

    private class DeleteArtistAsyncTask extends AsyncTask<Artist, Void, Void> {

        ArtistsDao artistsDao;

        DeleteArtistAsyncTask(ArtistsDao artistsDao) {
            this.artistsDao = artistsDao;
        }

        @Override
        protected Void doInBackground(Artist... artists) {
            artistsDao.delete(artists[0].getArtistId());
            return null;
        }
    }

    private class DeleteAllArtistAsyncTask extends AsyncTask<Void, Void, Void> {

        ArtistsDao artistsDao;

        DeleteAllArtistAsyncTask(ArtistsDao artistsDao) {
            this.artistsDao = artistsDao;
        }

        @Override
        protected Void doInBackground(Void... params) {
            artistsDao.deleteAll();
            return null;
        }
    }

    private class GetAllArtistsAsyncTask extends AsyncTask<Void, Void, List<Artist>> {

        ArtistsDao artistsDao;

        GetAllArtistsAsyncTask(ArtistsDao artistsDao) {
            this.artistsDao = artistsDao;
        }

        @Override
        protected List<Artist> doInBackground(Void... params) {
            return artistsDao.getAllData();
        }
    }

    private class ContainsArtistAsyncTask extends AsyncTask<String, Void, Boolean> {

        ArtistsDao artistsDao;

        ContainsArtistAsyncTask(ArtistsDao artistsDao) {
            this.artistsDao = artistsDao;
        }

        @Override
        protected Boolean doInBackground(String... params) {
            return artistsDao.containsName(params[0]) != 0;
        }
    }


}
