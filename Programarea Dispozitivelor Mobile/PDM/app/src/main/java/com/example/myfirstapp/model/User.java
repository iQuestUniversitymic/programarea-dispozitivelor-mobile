package com.example.myfirstapp.model;

public class User {

    private String username;
    private String password;
    private Token token;

    public User(String username, String password) {
        this.username = username;
        this.password = password;
    }

    public User(String username, String password, String token) {
        this.username = username;
        this.password = password;
        this.token.setToken(token);
    }

    public User() {

    }

    public String getUsername() {
        return this.username;
    }

    public void setUsername(String username) {
        this.username = username;
    }

    public String getPassword() {
        return this.password;
    }

    public void setPassword(String password) {
        this.password = password;
    }

    public String getToken() {
        return this.token.getToken();
    }

    public void setToken(String token) {
        this.token.setToken(token);
    }


}
