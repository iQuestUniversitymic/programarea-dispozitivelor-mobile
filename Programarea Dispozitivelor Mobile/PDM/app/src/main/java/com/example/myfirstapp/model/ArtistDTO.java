package com.example.myfirstapp.model;

import java.io.Serializable;
import java.util.ArrayList;

public class ArtistDTO {

    private String artistName;

    public ArtistDTO(String artistName) {
        this.artistName = artistName;
    }

    public String getArtistName() {
        return artistName;
    }

    public void setArtistName(String artistName) {
        this.artistName = artistName;
    }
}
