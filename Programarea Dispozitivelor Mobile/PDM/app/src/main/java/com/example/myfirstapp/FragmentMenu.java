package com.example.myfirstapp;

import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;

public class FragmentMenu extends Fragment {

    private static final String TAG = "FragmentMenu";

    private Button btnMyIdols;
    private Button btnAddIdol;

    @Nullable
    @Override
    public View onCreateView(@NonNull LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        View view = inflater.inflate(R.layout.fragment_menu_layout, container, false);
        btnMyIdols = view.findViewById(R.id.button);
        btnAddIdol = view.findViewById(R.id.button2);

        Log.d(TAG, "onCreateView: started.");

        btnMyIdols.setOnClickListener(v -> ((ArtistsActivity) getActivity()).setViewPager(1));
        btnAddIdol.setOnClickListener(v -> ((ArtistsActivity) getActivity()).setViewPager(2));

        return view;
    }
}
