package com.example.myfirstapp;

import android.support.annotation.NonNull;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import com.example.myfirstapp.model.Artist;

import java.util.ArrayList;
import java.util.List;

public class ArtistAdapter extends RecyclerView.Adapter<ArtistAdapter.ArtistHolder> {

    private List<Artist> artists = new ArrayList<>();

    @NonNull
    @Override
    public ArtistHolder onCreateViewHolder(@NonNull ViewGroup viewGroup, int i) {

        View itemView = LayoutInflater.from(viewGroup.getContext())
                .inflate(R.layout.artist_item, viewGroup, false);

        return new ArtistHolder(itemView);
    }

    @Override
    public void onBindViewHolder(@NonNull ArtistHolder artistHolder, int i) {
        Artist currentArtist = artists.get(i);
        artistHolder.textViewName.setText(currentArtist.getArtistName());
    }

    @Override
    public int getItemCount() {
        return artists.size();
    }


    public void setArtists(List<Artist> artists){
        this.artists = artists;
        notifyDataSetChanged();
    }

    class ArtistHolder extends RecyclerView.ViewHolder{


        private TextView textViewId;
        private TextView textViewName;

        public ArtistHolder(@NonNull View itemView) {
            super(itemView);
            textViewName = itemView.findViewById(R.id.text_view_artist_name);
        }



    }


}
