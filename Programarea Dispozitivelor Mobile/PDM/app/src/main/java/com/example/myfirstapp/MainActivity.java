package com.example.myfirstapp;

import android.content.Context;
import android.content.Intent;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.widget.EditText;
import android.widget.Toast;

import com.example.myfirstapp.model.Artist;
import com.example.myfirstapp.model.ArtistDTO;
import com.example.myfirstapp.model.Token;
import com.example.myfirstapp.model.UserDTO;
import com.example.myfirstapp.service.APIService;

import java.util.ArrayList;
import java.util.List;
import java.util.concurrent.TimeUnit;

import okhttp3.OkHttpClient;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;
import retrofit2.Retrofit;
import retrofit2.converter.gson.GsonConverterFactory;

public class MainActivity extends AppCompatActivity {

    static Context context;
    static String authToken;

    OkHttpClient client = new OkHttpClient.Builder()
            .connectTimeout(10, TimeUnit.SECONDS)
            .writeTimeout(10, TimeUnit.SECONDS)
            .readTimeout(10, TimeUnit.SECONDS)
            .build();

    Retrofit.Builder builder = new Retrofit.Builder()
            .baseUrl("http://192.168.43.125:44397/")
            .client(client)
            .addConverterFactory(GsonConverterFactory.create());

    Retrofit retrofit = builder.build();

    APIService APIService = retrofit.create(APIService.class);

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        getSupportActionBar().hide();
        context = this;

        findViewById(R.id.cardView).setOnClickListener((view) -> login());

    }

    private void login() {

        final EditText username = findViewById(R.id.editText);
        final EditText password = findViewById(R.id.editText2);

        UserDTO userDTO = new UserDTO(username.getText().toString(), password.getText().toString());
        Call<Token> call = APIService.login(userDTO);


        call.enqueue(new Callback<Token>() {
            @Override
            public void onResponse(Call<Token> call, Response<Token> response) {
                if (response.isSuccessful()) {
                    Toast.makeText(MainActivity.this, "Let's check your idols out!", Toast.LENGTH_SHORT).show();
                    authToken = response.body().getToken();

                    Intent intent = new Intent(context, ArtistsActivity.class);
                    intent.putExtra("authToken", authToken);
                    startActivity(intent);

                } else {
                    Toast.makeText(MainActivity.this, "Login failed!", Toast.LENGTH_SHORT).show();
                }
            }

            @Override
            public void onFailure(Call<Token> call, Throwable t) {
                System.out.println(t.getCause());
                Toast.makeText(MainActivity.this, "An error ocurred while authenticating!", Toast.LENGTH_SHORT).show();
            }
        });

    }

}
