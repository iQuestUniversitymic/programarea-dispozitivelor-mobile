package com.example.myfirstapp.Database;

import android.app.Application;
import android.arch.lifecycle.AndroidViewModel;
import android.arch.lifecycle.LiveData;
import android.support.annotation.NonNull;

import com.example.myfirstapp.model.Artist;

import java.util.List;
import java.util.concurrent.ExecutionException;

public class ArtistViewModel extends AndroidViewModel {

    private ArtistRepository artistRepository;
    private LiveData<List<Artist>> artists;

    public ArtistViewModel(@NonNull Application application) {
        super(application);
        artistRepository = new ArtistRepository(application);
        artists = artistRepository.getAll();
    }

    public void insert(Artist artist) {
        artistRepository.insert(artist);
    }

    public void update(Artist artist) {
        artistRepository.update(artist);
    }

    public void delete(Artist artist) {
        artistRepository.delete(artist);
    }

    public synchronized void deleteAll() {
        artistRepository.deleteAll();
    }

    public synchronized void add(String artistName) {
        artistRepository.add(artistName);
    }

    public synchronized LiveData<List<Artist>> getAll() {
        return artists;
    }

    public synchronized List<Artist>  getArtistsFromDb() throws ExecutionException, InterruptedException {
        return artistRepository.getAllData();
    }

    public synchronized Boolean containsArtist(String artistName) throws ExecutionException, InterruptedException {
        return artistRepository.contains(artistName);
    }

}
