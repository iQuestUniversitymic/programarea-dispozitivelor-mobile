import React from 'react';
import { StyleSheet, Text, View} from 'react-native';
import { createStackNavigator, createAppContainer } from "react-navigation";
import Login from "./components/Login"
import Artists from "./components/Artists"

const Application = createStackNavigator(
  
    {
      Home : {screen : Login },
      Artists: {screen : Artists},
    },
    {
      defaultNavigationOptions : {
        title : '',
        headerStyle:{
          backgroundColor: '#1a237e'
        }
    }

});

const AppContainer = createAppContainer(Application);

export default class App extends React.Component {
  render() {
    return (
      <AppContainer/>
      );
  }
}


const styles = StyleSheet.create({
  container: {
    flex: 1,
    backgroundColor: '#fff',
    alignItems: 'center',
    justifyContent: 'center',
  }
});
