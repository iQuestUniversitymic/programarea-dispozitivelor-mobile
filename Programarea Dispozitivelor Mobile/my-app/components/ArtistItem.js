import React from 'react';
import { StyleSheet, Text, View, ScrollView, TouchableOpacity, TextInput} from 'react-native';

export default class ArtistItem extends React.Component {

    render() {
    return (

        <View style={styles.item}>
            <Text style={styles.itemText}>{this.props.val}</Text>
        </View>


    );
    }
}
  const styles = StyleSheet.create({

    item: {
        position: 'relative',
        padding: 20,
        paddingRight: 100,
        borderBottomWidth: 2,
        borderBottomColor: '#ededed',
    },

    itemText: {
        paddingLeft: 20,
        borderLeftWidth: 10,
        borderLeftColor: '#7e57c2'
    },

});
