import React from 'react';
import { StyleSheet, Text, View, ScrollView, TouchableOpacity, TextInput, AsyncStorage, NetInfo, ToastAndroid} from 'react-native';
import ArtistItem from './ArtistItem';

export default class Artists extends React.Component {


    constructor(props){
        super(props);
        this.state = {
            artistArray: '',
            artistNameInput: '',
            token: '',
            offlineArtists: []
        }
    }

    componentDidMount(){
        this.getArtistsObject().then((result) => this.setState({
            artistArray: JSON.parse(result)
        }))

        this.getToken().then((result) => this.setState({
            token: result
        }))

        this.SynchronizeData();
    }

    postOfflineArtists = () => {
        console.log('Sincronizare....');
        NetInfo.isConnected.fetch().then(isConnected => {
        
            if (isConnected && this.state.offlineArtists.length > 0) {
                                
                this.state.offlineArtists.forEach(artist => {

                    if(this.state.token){
                        
                        fetch('http://192.168.43.125:44397/artists', {
            
                            method: 'POST',
                            headers: {
                                'Content-Type': 'application/json',
                                'Authorization' : 'Bearer ' + this.state.token
                            },
                            body: JSON.stringify({
                                artistName: artist.artistName
                            })
                        })
                            .then((response) => {
                                console.log(this.state.token);
                                response.json();
                                
                                console.log(response);
            
                                if (response.status === 200) {
                                    console.log("S-A ADAUGAT");
                                }
            
                            }).done();
                    }
                })

                this.setState({offlineArtists: []});
            }
        }
    )}

    getArtistsObject = async () => {
        return AsyncStorage.getItem('artists');
    }

    getToken = async () => {
        return AsyncStorage.getItem('token');
    }

    static navigationOptions = {
        title: 'iDols',
        headerTitleStyle: {
            fontWeight: 'bold',
            color: 'white'
          },
        headerLeft: null
    };

    SynchronizeData=()=>{
        
        setInterval(this.postOfflineArtists, 20000);
    }

    render() {

        if (!this.state.artistArray.length) {
            return null;
          }

        let artists = this.state.artistArray.map((artist, i) => (
            <ArtistItem key={i} val={artist.artistName} />
            ))


    return (
            <View style={styles.container}>
                <ScrollView style={styles.scrollContainer}>
                    {artists}
                </ScrollView>

                <View style={styles.footer}>
                    <TextInput 
                        style = {styles.textInput}
                        placeholder = "--> iDol name"
                        placeholderTextColor = "white"
                        underlineColorAndroid = "transparent"
                        value={this.state.artistNameInput}
                        onChangeText = {(text) => this.setState({artistNameInput : text})}>
                    </TextInput>
                </View>

                <TouchableOpacity onPress={this.addArtist.bind(this)} style={styles.addButton}>
                    <Text style={styles.addButtonText}>+</Text>
                </TouchableOpacity>
            </View>
            );
    }

addArtist() {

    if (this.state.artistNameInput) {
        
        var artistID = this.state.artistArray[this.state.artistArray.length-1].artistId;
        artistID++;

        var newArtist = {
            "artistId" : artistID ,
            "artistName" : this.state.artistNameInput
        };

        this.state.artistArray.push(newArtist);

        var artistToSend = this.state.artistNameInput;
        NetInfo.isConnected.fetch().then(isConnected => {
            
           if (isConnected) {
                
                if(this.state.token){
                    
                    fetch('http://192.168.43.125:44397/artists', {
        
                        method: 'POST',
                        headers: {
                            'Content-Type': 'application/json',
                            'Authorization' : 'Bearer ' + this.state.token
                        },
                        body: JSON.stringify({
                            artistName: artistToSend
                        })
                    })
                        .then((response) => {
                            console.log(this.state.token);
                            response.json();
                            
                            console.log(response);
        
                            if (response.status === 200) {
                                console.log("S-A ADAUGAT");
                            }
        
                        }).done();
                }
            }
                else{
                    alert("The Internet connection is off!");
                    this.state.offlineArtists.push(newArtist);
                    this.setState({ offlineArtists: this.state.offlineArtists });
                }
        });

        this.setState({ artistArray: this.state.artistArray });
        this.setState({ artistNameInput: ''});    
        console.log(this.state.offlineArtists);

    }

}

}

  const styles = StyleSheet.create({

    container: {
        flex: 1
    },
    scrollContainer: {
        flex: 1,
        marginBottom: 90,
    },
    footer: {
        position: 'absolute',
        bottom: 0,
        left: 0,
        right: 0,
        zIndex: 10,
    },
    textInput: {
        alignSelf: 'stretch',
        color: 'white',
        padding: 20,
        backgroundColor: '#252525',
        borderTopWidth: 2,
        borderTopColor: '#ededed',
    },
    addButton: {
        position: 'absolute',
        zIndex: 20,
        right: 10,
        bottom: 90,
        backgroundColor: '#0d47a1',
        width: 75,
        height: 75,
        borderRadius: 50,
        alignItems: 'center',
        justifyContent: 'center',
        elevation: 8,
    },
    addButtonText: {
        color: "white",
        fontSize: 24
    }
});
