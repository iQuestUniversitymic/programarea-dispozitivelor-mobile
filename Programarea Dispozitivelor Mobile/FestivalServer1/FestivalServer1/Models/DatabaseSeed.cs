﻿using Microsoft.AspNetCore.Identity;
using Microsoft.Extensions.DependencyInjection;
using System;
using System.Linq;

namespace FestivalServer1.Models
{
    public class DatabaseSeed
    {
        public static void Initialize(IServiceProvider serviceProvider)
        {
            var context = serviceProvider.GetRequiredService<UserContext>();
            var userManager = serviceProvider.GetRequiredService<UserManager<User>>();

            context.Database.EnsureCreated();

            if (!context.Users.Any())
            {
                User user = new User()
                {
                    Email = "ionut@a.com",
                    SecurityStamp = Guid.NewGuid().ToString(),
                    UserName = "ionut"
                };

                userManager.CreateAsync(user, "Password@123");
            }
        }
    }
}
