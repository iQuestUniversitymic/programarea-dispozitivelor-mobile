﻿using System;
using System.Collections.Generic;
using System.Threading.Tasks;
using FestivalServer1.Models;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Mvc;
using Microsoft.EntityFrameworkCore;

namespace FestivalServer1.Controllers
{   
    [Authorize]
    [Route("artists")]
    [ApiController]
    public class ArtistController : ControllerBase
    {
        private readonly FestivalContext _context;
        private readonly UserContext _userContext;

        public ArtistController(FestivalContext context, UserContext userContext)
        {
            _context = context;
            _userContext = userContext;
        }

        //GET: /artists
        [HttpGet]
        public async Task<ActionResult<IEnumerable<Artist>>> GetArtists()
        {
            return await _context.Artists.ToListAsync();
        }

        //GET: /artists/id
        [HttpGet("{id}")]
        public async Task<ActionResult<Artist>> GetArtist(int id)
        {
            var artist = await _context.Artists.FindAsync(id);

            if (artist == null)
            {
                return NotFound();
            }

            return artist;
        }

        //POST: /artists/id
        [HttpPost]
        public async Task<ActionResult<Artist>> PostArtist(Artist artist)
        {
            try
            {
                _context.Artists.Add(artist);
                await _context.SaveChangesAsync();
            }
            catch (Exception)
            {
                return NotFound();
            }

            return Ok();
        }

        //DELETE: /artists/id
        [HttpDelete("{id}")]
        public async Task<ActionResult<Artist>> DeleteArtist(int id)
        {
            var artist = await _context.Artists.FindAsync(id);
            if (artist == null)
            {
                return NotFound();
            }

            _context.Artists.Remove(artist);
            await _context.SaveChangesAsync();

            return Ok();
        }

        //PUT: /artists/id
        [HttpPut("{id}")]
        public async Task<IActionResult> PutArtist(int id, Artist newArtist)
        {
            if (id != newArtist.ArtistId)
            {
                return BadRequest();
            }

            _context.Entry(newArtist).State = EntityState.Modified;
            await _context.SaveChangesAsync();

            return Ok();

        }
    }
}
